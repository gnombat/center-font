# center-font.py

This program displays the CSS padding needed to [vertically center a font](https://tonsky.me/blog/centering/#what-can-be-done-web-developers).

## Requirements

This program requires [fonttools](https://github.com/fonttools/fonttools).

You can probably install this using your OS package manager.
For example, on Debian or Ubuntu:

```
apt install python3-fonttools
```

Alternatively, you can use PyPI:

```
pip install fonttools
```

## Example

Download [Noto Sans from Google Fonts](https://fonts.google.com/noto/specimen/Noto+Sans).

```
$ unzip -q Noto_Sans.zip -d Noto_Sans
$ ./center-font.py Noto_Sans/static/NotoSans-Regular.ttf
padding-bottom: 0.062em;
$ ./center-font.py --verbose Noto_Sans/static/NotoSans-Regular.ttf
Units per em = 1000
Ascender = 1069
Descender = -293
Cap height = 714
padding-bottom: 0.062em;
```

## License

This program is free software. See the file "LICENSE.txt" for license terms.
