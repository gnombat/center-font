#!/usr/bin/env python3

import argparse

from fontTools import ttLib

def main():
  parser = argparse.ArgumentParser(description = 'Generate CSS to vertically center a font.')
  parser.add_argument('FONT_FILE', help = 'the font file')
  parser.add_argument('-v', '--verbose', help = 'explain what is being done', action = 'count', default = 0)

  args = parser.parse_args()

  font = ttLib.TTFont(args.FONT_FILE)
  units_per_em = font['head'].unitsPerEm

  # Note that font['hhea'].ascent and font['hhea'].descent are not the same as font['OS/2'].sTypoAscender and font['OS/2'].sTypoDescender.
  # The values may be entirely different.
  # There are also font['OS/2'].usWinAscent and font['OS/2'].usWinDescent,
  # which contain essentially the same information as font['hhea'].ascent and font['hhea'].descent;
  # however, font['OS/2'].usWinDescent has the sign reversed (font['OS/2'].usWinDescent is positive while font['hhea'].descent is negative).
  # See https://learn.microsoft.com/en-us/typography/opentype/spec/os2 for more information.
  ascender = font['hhea'].ascent
  descender = font['hhea'].descent
  cap_height = font['OS/2'].sCapHeight

  if args.verbose:
    print('Units per em = ' + str(units_per_em))
    print('Ascender = ' + str(ascender))
    print('Descender = ' + str(descender))
    print('Cap height = ' + str(cap_height))

  padding_bottom = (ascender - cap_height + descender) / units_per_em
  if padding_bottom >= 0:
    position = 'bottom'
    padding = padding_bottom
  else:
    position = 'top'
    padding = - padding_bottom

  padding = '%.5f' % padding

  # simplify by trimming any trailing zeros
  padding = padding.rstrip('0')
  padding = padding.rstrip('.')

  print('padding-' + position + ': ' + padding + 'em;')

if __name__ == '__main__':
  main()
